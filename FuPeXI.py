#!/usr/bin/env python

"""
FuPeXI - Fusion peptide extractor and Informer

Originally was developed:
Date: 2015-09-03
By: Anne-Mette Bjerregaard

Edited:
Date: 2018-02-25
By: Rotem Barzilay

FuPeXI.py extracts user defined peptides lengths around fusion poinys.
Information from each mutation is annotated together with the mutant and normal peptides in the file output. 
"""

# Import modules 
from collections import defaultdict, namedtuple
from datetime import datetime
from itertools import izip
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna
from Bio import BiopythonWarning
from ConfigParser import SafeConfigParser
from tempfile import NamedTemporaryFile
import sys, re, getopt, itertools, warnings, string, subprocess, os.path, math, tempfile, shutil, numpy, pandas
import math

import csv


def main(args):
    start_time = datetime.now()

    # State input in variables
    input_ = read_options(args)
    version = '1.1.3'

    # Redirect std error when run on webserver 
    webserver_err_redirection(input_.webserver)

    # Check input file paths 
    species = define_species(input_.species)
    peptide_length = extract_peptide_length(input_.peptide_length)
    paths = check_input_paths(input_, peptide_length, species)
    tmp_dir = create_tmp_dir()
    www_tmp_dir = create_webserver_tmp_dir(input_.webserver)

    # Read in data 
    print_ifnot_webserver('\nReading in data', input_.webserver)
    proteome_reference, sequence_count = build_proteome_reference(paths.proteome_ref_file, input_.webserver, species)
    genome_reference = build_genome_reference(paths.genome_ref_file, input_.webserver, species)

    """load fusion info"""
    cds_start_db = load_cds_start_db(input_.config)
    fusion_info = load_fusion_info(input_.fusion_file)


    """
    MuPeX: Mutant peptide extraction
    Extracting user defined peptides lengths around missense variant mutations, indels and frameshifts
    """
    start_time_mupex = datetime.now()
    print_ifnot_webserver('\nMuPeX: Starting mutant peptide extraction', input_.webserver)


    # Extract reference peptides
    reference_peptides, reference_peptide_counters, reference_peptide_file_names = reference_peptide_extraction(proteome_reference, peptide_length, tmp_dir, input_.webserver, input_.keep_temp, input_.prefix, input_.outdir, input_.config)

    # extract mutant peptides
    peptide_info, peptide_counters, fasta_printout, pepmatch_file_names = peptide_extraction_fork(peptide_length, fusion_info, genome_reference, cds_start_db, reference_peptides, reference_peptide_file_names, input_.fasta_file_name, paths.peptide_match, tmp_dir, input_.webserver, input_.print_mismatch, input_.keep_temp, input_.prefix, input_.outdir, input_.num_mismatches)

    # print fasta file
    fasta_file = write_fasta(tmp_dir, fasta_printout, input_.webserver)

    end_time_mupex = datetime.now()



    """
    MuPeI: Mutant peptide Informer
    Information from each mutation is annotated together with the mutant and normal peptides in the file output
    """
    start_time_mupei = datetime.now()
    print_ifnot_webserver('\nMuPeI: Starting mutant peptide informer', input_.webserver)

    # run netMHCpan
    unique_mutant_peptide_count, peptide_file = write_peptide_file(peptide_info, tmp_dir, input_.webserver, input_.keep_temp, input_.prefix, input_.outdir)
    netMHCpan_runtime, unique_alleles, netMHC_file = run_netMHCpan(input_.HLA_alleles, paths.netMHC, peptide_file, tmp_dir, input_.webserver, input_.keep_temp, input_.prefix, input_.outdir)
    net_mhc = build_netMHC(netMHC_file, input_.webserver)

    # write files
    output_file = write_output_file_fork(peptide_info, net_mhc, unique_alleles, tmp_dir, input_.webserver, input_.print_mismatch, reference_peptides, proteome_reference, version)
    log_file = write_log_file_fork(sys.argv, peptide_length, sequence_count, reference_peptide_counters, None, peptide_counters, start_time_mupex, start_time_mupei, start_time, end_time_mupex, input_.HLA_alleles, netMHCpan_runtime, unique_mutant_peptide_count, unique_alleles, tmp_dir, input_.webserver, version)

    # clean up
    move_output_files(input_.outdir, log_file, input_.logfile, fasta_file, input_.fasta_file_name, output_file, input_.output, input_.webserver, www_tmp_dir)
    clean_up(tmp_dir)

    webserver_print_output(input_.webserver, www_tmp_dir, input_.output, input_.logfile, input_.fasta_file_name)


def load_cds_start_db(config):
    cds_db_path = config_parse(config, 'References', 'cds')
    db_dict = {}
    with open(cds_db_path,'r') as in_fd:
        reader = csv.DictReader(in_fd, delimiter=',')
        for row in reader:
            # create dicts in needed:
            if row['gene_id'] not in db_dict:
                db_dict[row['gene_id']] = {}
            if row['trans_id'] not in db_dict[row['gene_id']]:
                db_dict[row['gene_id']][row['trans_id']] = {}
            # fill dict:
            db_dict[row['gene_id']][row['trans_id']] = {'orientation': row['orientation'], 'relative_start': int(row['relative_start'])}
    return db_dict


def load_fusion_info(file_path):
    fusion_info = []
    Fusion_Info = namedtuple('fusion_info', ['row_idx', 'gene1_symbol', 'gene2_symbol', 'fuse_desc',
                                             'common_mapping_reads_count', 'spanning_pairs', 'spanning_unique_pairs',
                                             'longest_anchor_found', 'finding_method', 'fusion_point1', 'fusion_point2',
                                             'gene1', 'gene2', 'exon1', 'exon2', 'fusion_sequence', 'predicted_effect',
                                             'predicted_fused_transcript', 'trans1', 'trans1_end_pos', 'trans2',
                                             'trans2_start_pos', 'fused_protein'])
    with open(file_path, 'r') as f_in:
        dict_reader = csv.DictReader(f_in, delimiter='\t')
        for (row_idx, dict_row) in enumerate(dict_reader):
            gene1_symbol = dict_row['Gene_1_symbol(5end_fusion_partner)']
            gene2_symbol = dict_row['Gene_2_symbol(3end_fusion_partner)']
            fuse_desc = dict_row['Fusion_description']
            common_mapping_reads_count = dict_row['Counts_of_common_mapping_reads']
            spanning_pairs = dict_row['Spanning_pairs']
            spanning_unique_pairs = dict_row['Spanning_unique_reads']
            longest_anchor_found = dict_row['Longest_anchor_found']
            finding_method = dict_row['Fusion_finding_method']
            fusion_point1 = dict_row['Fusion_point_for_gene_1(5end_fusion_partner)']
            fusion_point2 = dict_row['Fusion_point_for_gene_2(3end_fusion_partner)']
            gene1 = dict_row['Gene_1_id(5end_fusion_partner)']
            gene2 = dict_row['Gene_2_id(3end_fusion_partner)']
            exon1 = dict_row['Exon_1_id(5end_fusion_partner)']
            exon2 = dict_row['Exon_2_id(3end_fusion_partner)']
            fusion_sequence = dict_row['Fusion_sequence']
            predicted_effect = dict_row['Predicted_effect']
            # separating concated values:
            transcripts = dict_row['Predicted_fused_transcripts']
            fused_proteins = dict_row['Predicted_fused_proteins']
            fused_proteins_list = fused_proteins.split(';')
            if not transcripts or not fused_proteins:
                continue
            for (i, transcript_pair_str) in enumerate(transcripts.split(';')):
                transcript_pair = transcript_pair_str.split('/')
                trans1_split = transcript_pair[0].split(':')
                trans2_split = transcript_pair[1].split(':')
                trans1 = trans1_split[0]
                trans1_end_pos = int(trans1_split[1])
                trans2 = trans2_split[0]
                trans2_start_pos = int(trans2_split[1])
                fused_protein = fused_proteins_list[i]
                fusion_info.append(Fusion_Info(row_idx+1, gene1_symbol, gene2_symbol, fuse_desc,
                                             common_mapping_reads_count, spanning_pairs, spanning_unique_pairs,
                                             longest_anchor_found, finding_method, fusion_point1, fusion_point2,
                                             gene1, gene2, exon1, exon2, fusion_sequence, predicted_effect,
                                             transcript_pair_str, trans1, trans1_end_pos, trans2, trans2_start_pos, fused_protein))
    return fusion_info




"""

CHECK FILE PATHS

"""


def check_input_paths(input_, peptide_lengths, species):
    file_names = defaultdict(dict) # empty dictionary
    category_list, id_list = create_lits_for_path_checkup(peptide_lengths)

    # parse files stated in config file 
    for category, ID in izip(category_list, id_list):
        file_path = config_parse(input_.config, category, ID)
        check_path(file_path)
        file_names[ID] = file_path

    # Exit program if genome or proteome reference is not stated 
    if file_names['cDNA'] == None:
        usage(); sys.exit('cDNA reference file is missing!')
    if file_names['pep'] == None:
        usage(); sys.exit('Proteome reference file is missing!')

    check_fusion_file(input_.fusion_file)
    check_path(input_.outdir)

    # Create and fill named tuple
    Paths = namedtuple('files', ['gene_symbol_file', 'genome_ref_file', 'proteome_ref_file', 'netMHC', 'peptide_match'])
    paths = Paths(file_names['symbol'], file_names['cDNA'], file_names['pep'], file_names['MHC'], file_names['PM'])

    return paths


FUSION_CATCHER_HEADER_VALUES = set(['Gene_1_symbol(5end_fusion_partner)', 'Gene_2_symbol(3end_fusion_partner)', 'Fusion_description', 'Counts_of_common_mapping_reads', 'Spanning_pairs', 'Spanning_unique_reads', 'Longest_anchor_found', 'Fusion_finding_method', 'Fusion_point_for_gene_1(5end_fusion_partner)', 'Fusion_point_for_gene_2(3end_fusion_partner)', 'Gene_1_id(5end_fusion_partner)', 'Gene_2_id(3end_fusion_partner)', 'Exon_1_id(5end_fusion_partner)', 'Exon_2_id(3end_fusion_partner)', 'Fusion_sequence', 'Predicted_effect', 'Predicted_fused_transcripts', 'Predicted_fused_proteins'])
def check_fusion_file(fusion_file):
    with open(fusion_file) as f_in:
        header_line = f_in.readline()
        header_line = header_line.rstrip('\n')
        header_values = set(header_line.split('\t'))
        # print(header_values)
        # print(FUSION_CATCHER_HEADER_VALUES)
        if header_values != FUSION_CATCHER_HEADER_VALUES:
            sys.exit('ERROR: {} file is not a FusionCatcher output file\n'.format(fusion_file))
        # print(header_line)

def create_lits_for_path_checkup(peptide_lengths):
    for length in peptide_lengths:
        category_list = ['netMHC','PeptideMatch', 'References', 'References']
        id_list = ['MHC', 'PM', 'cDNA', 'pep']
        category_list.append('References')
        id_list.append('pep' + str(length))
    return category_list, id_list


def config_parse(config_file, category, ID):
    # check if config.ini file exists 
    if os.path.exists(config_file) == False :
        usage(); sys.exit('ERROR: Path to OR config.ini does not exist!\nERROR: Use -c option to specify path/to/config.ini file\n')

    # parse config file
    config = SafeConfigParser()
    config.read(config_file)
    path = config.get(category, ID) if config.has_option(category, ID) else None

    return path


def check_path(path):
    if not path == None:
        if os.path.exists(path) == False:
            usage(); sys.exit('ERROR: {} path or file does not exist\n'.format(path))


def check_file_size(webserver, input_file_path, file_tag):
    if not webserver == None:
        if os.path.getsize(input_file_path) > 20000000 :
            sys.exit('STOP: The input {} size exceeds the limit of 20M\n\tPlease check your file\n\tIf correct use the command-line tool instead or contact us: ambj@cbs.dtu.dk or eklund@cbs.dtu.dk'.format(file_tag))



def create_tmp_dir():
    tmp_dir = tempfile.mkdtemp(prefix = 'FuPeXI_')
    os.system('chmod -R a+rwx {}'.format(tmp_dir)) 
    return tmp_dir



def print_ifnot_webserver(string, webserver):
    if webserver == None :
        print(string)



def create_webserver_tmp_dir(webserver):
    if not webserver == None :
        www_tmp_dir = tempfile.mkdtemp(prefix = 'FuPeXI_', dir='/usr/opt/www/pub/CBS/services/FuPeXI-1.1/tmp')
    else:
        www_tmp_dir = None
    return www_tmp_dir


def webserver_err_redirection(webserver):
    if not webserver == None: 
        sys.stderr = sys.stdout




"""

READ IN DATA

"""

def build_proteome_reference(proteome_ref_file, webserver, species):
    print_ifnot_webserver('\tCreating proteome reference dictionary', webserver)
    proteome_reference = defaultdict(dict) # empty dictionary 
    sequence_count = 0 # sequence count

    with open(proteome_ref_file) as f:
        for line in f.readlines(): 
            if line.startswith('>'): # fasta header (>)
                # test species compatibility 
                if not species.gene_id_prefix in line:
                    usage(); sys.exit('ERROR: species prefix {} for {} not found in proteome reference file\nINFO: use --species option if another species than {} is used'.format(species.gene_id_prefix, species.species, species.species))
                # Save gene and transcript Ensembl ID (re = regular expression)
                geneID = re.search(r'gene:({}\d+)'.format(species.gene_id_prefix), line).group(1).strip()
                transID = re.search(r'transcript:({}\d+)'.format(species.trans_id_prefix), line).group(1).strip()
                # Insert gene and transcript ID in directory, assign empty value
                proteome_reference[geneID][transID] = ""
                sequence_count += 1
            else:
                # Assign amino acid sequence as value in directory (the following lines until next header)
                proteome_reference[geneID][transID] += line.strip()
    return proteome_reference, sequence_count



def build_genome_reference(genome_ref_file, webserver, species):
    print_ifnot_webserver('\tCreating genome reference dictionary', webserver)
    genome_reference = defaultdict(dict) # empty dictionary 

    with open(genome_ref_file) as f:
        for line in f.readlines(): 
            if line.startswith('>'): # fasta header (>)
                # test species compatibility 
                if not species.gene_id_prefix in line:
                    usage(); sys.exit('ERROR: species prefix {} for {} not found in cDNA reference file\nINFO: use --species option if another species than {} is used'.format(species.gene_id_prefix, species.species, species.species))
                # Save gene and transcript Ensembl ID (re = regular expression)
                geneID = re.search(r'gene:({}\d+)'.format(species.gene_id_prefix), line).group(1).strip()
                transID = re.search(r'>({}\d+)'.format(species.trans_id_prefix), line).group(1).strip()
                # Insert gene and transcript ID in directory, assign empty value
                genome_reference[geneID][transID] = ""
            else:
                # Assign amino acid sequence as value in directory (the following lines until next header)
                genome_reference[geneID][transID] += line.strip()
    return genome_reference


def define_species(species):
    if species == 'human' :
        trans_id_prefix = 'ENST'
        gene_id_prefix = 'ENSG'
        assembly = 'GRCh38'
        species = 'homo_sapiens'
    elif species == 'mouse' :
        trans_id_prefix = 'ENSMUST'
        gene_id_prefix = 'ENSMUSG'
        assembly = 'GRCm38'
        species = 'mus_musculus'
    else :
        usage(); sys.exit('ERROR:\tSpecies {} not recognized \n'.format(species))

    Species = namedtuple('species', ['gene_id_prefix', 'trans_id_prefix', 'assembly', 'species'])
    species = Species(gene_id_prefix, trans_id_prefix, assembly, species)

    return species


def extract_peptide_length(peptide_length):
    peptide_length_list = list() 
    if isinstance( peptide_length, int ):
        peptide_length_list.append(peptide_length)
    else:
        if '-' in peptide_length:
            length_range = map(int,peptide_length.split('-'))
            assert length_range[1] > length_range[0], 'peptide length range should be stated from lowest to highest. your input: {}'.format(peptide_length)
            peptide_length_list = range(length_range[0], length_range[1] + 1)
        elif ',' in peptide_length:
            peptide_length_list = map(int, peptide_length.split(','))
        else:
            peptide_length_list.append(int(peptide_length))
    return peptide_length_list



"""

FuPeXI

"""



def reference_peptide_extraction(proteome_reference, peptide_length, tmp_dir, webserver, keep_tmp, file_prefix, outdir, config_file):
    print_ifnot_webserver('\tExtracting all possible peptides from reference', webserver)
    reference_peptides = set()
    reference_peptide_count = 0
    reference_peptide_file_names = defaultdict(dict) # empty dictionary

    # loop trough the proteome reference and chop up in all peptides 
    for length in peptide_length:
        # check if peptide references are stated in config file
        reference_peptide_file_path = config_parse(config_file, 'References', 'pep' + str(length))
        # Create peptide reference file if not stated in the config file
        if not reference_peptide_file_path == None: 
            print_ifnot_webserver('\t\tPeptide reference file of {} aa are stated in config file'.format(length), webserver)
            reference_peptide_file_names[length] = reference_peptide_file_path
            with open(reference_peptide_file_path) as f:
                for line in f.readlines():
                    reference_peptides.add(line.strip())
                    reference_peptide_count += 1
        else:
            reference_length_peptides = set()
            print_ifnot_webserver('\t\tPeptides of {} aa are being extracted'.format(length), webserver)
            reference_peptides_file = NamedTemporaryFile(delete = False, dir = tmp_dir)
            for geneID in proteome_reference:
                for transID in proteome_reference[geneID]:
                    aa_sequence = proteome_reference[geneID][transID]
                    peptide_list  = chopchop(aa_sequence, length)
                    for pep in peptide_list:
                        if not len(pep) == length:
                            continue
                        if '*' in pep:
                            continue 
                        if 'X' in pep: 
                            continue
                        if 'U' in pep:
                            continue
                        reference_peptide_count += 1
                        reference_length_peptides.add(pep)
                        reference_peptides.add(pep)
            reference_peptides_file.write('{}\n'.format('\n'.join(reference_length_peptides)))
            reference_peptides_file.close()
            reference_peptide_file_names[length] = reference_peptides_file


    ReferencePetideCounters = namedtuple('reference_peptide_counters', ['total_peptide_count', 'unique_peptide_count'])
    reference_peptide_counters = ReferencePetideCounters(reference_peptide_count, len(reference_peptides))

    keep_temp_file(keep_tmp, 'txt', reference_peptide_file_names, file_prefix, outdir, peptide_length, 'reference_peptide')

    return reference_peptides, reference_peptide_counters, reference_peptide_file_names



def chopchop(aaSeq, peptide_length):
    peptides = []
    for i in range(len(aaSeq)):
        pep = aaSeq[i:i + peptide_length]
        if len(pep) < peptide_length:
            break
        peptides.append(pep)
    return peptides


def peptide_extraction_fork(peptide_lengths, fusion_info, genome_reference, cds_start_db, reference_peptides, reference_peptide_file_names, fasta_file_name, peptide_match, tmp_dir, webserver, print_mismatch, keep_tmp, file_prefix, outdir, num_mismatches):
    print_ifnot_webserver('\tPeptide extraction begun', webserver)
    peptide_count, normal_match_count, removal_count = 0, 0, 0
    peptide_info = defaultdict(dict) # empty dictionary
    fasta_printout = defaultdict(dict) if not fasta_file_name == None else None
    pepmatch_file_names = defaultdict(dict) # empty dictionary

    for p_length in peptide_lengths:
        mutated_peptides_missing_normal = set()
        for mutation_info in fusion_info:
            # create mutation counters
            intermediate_peptide_counters = {'mutation_peptide_count': 0, 'mutation_normal_match_count': 0, 'peptide_removal_count': 0}
            # extract sequence
            peptide_sequence_info = fusion_sequneces_creation(genome_reference, cds_start_db, mutation_info, p_length)

            if not peptide_sequence_info == None :
                if not fasta_printout == None:
                    fasta_printout = long_peptide_fasta_creation(peptide_sequence_info, mutation_info, fasta_printout)

                normpeps, mutpeps = chopchop(peptide_sequence_info.chop_normal_sequence, p_length), chopchop(peptide_sequence_info.mutation_sequence, p_length)
                # print('mutpeps: %s' % mutpeps)
                mutated_peptides_missing_normal = normal_peptide_identification(mutated_peptides_missing_normal, mutpeps, reference_peptides, mutation_info)
                peptide_mutation_position = peptide_mutation_position_annotation(mutpeps, peptide_sequence_info.mutation_position, p_length)
                peptide_info, intermediate_peptide_counters = peptide_selection(normpeps, mutpeps, peptide_mutation_position, intermediate_peptide_counters, peptide_sequence_info, peptide_info, mutation_info, p_length, reference_peptides)

            # Accumulate counters
            peptide_count += intermediate_peptide_counters['mutation_peptide_count']
            normal_match_count += intermediate_peptide_counters['mutation_normal_match_count']
            removal_count += intermediate_peptide_counters['peptide_removal_count']

        peptide_info, pepmatch_file_names = normal_peptide_correction(mutated_peptides_missing_normal, mutation_info, p_length, reference_peptide_file_names, peptide_info, peptide_match, tmp_dir, pepmatch_file_names, webserver, print_mismatch, num_mismatches)

    # Create and fill counter named-tuple
    PeptideCounters = namedtuple('peptide_counters', ['peptide_count', 'normal_match_count', 'removal_count'])
    peptide_counters = PeptideCounters(peptide_count, normal_match_count, removal_count)

    keep_temp_file(keep_tmp, 'txt', pepmatch_file_names, file_prefix, outdir, peptide_lengths, 'pepmatch')

    return peptide_info, peptide_counters, fasta_printout, pepmatch_file_names


def peptide_mutation_position_annotation(mutpeps, long_peptide_position, peptide_length):
    peptide_mutation_positions = []
    for i in range(len(mutpeps)):
        if type(long_peptide_position) is int:
            pep_mut_pos = long_peptide_position - i
        else:
            start = int(long_peptide_position.split(':')[0])
            end = int(long_peptide_position.split(':')[1])
            final_end = (end - i) if (end - i) < peptide_length else peptide_length
            final_start = (start - i) if (start - i) > 0 else 1
            pep_mut_pos = '{}:{}'.format(int(final_start),int(final_end))
        peptide_mutation_positions.append(pep_mut_pos)
    return peptide_mutation_positions



def peptide_selection (normpeps, mutpeps, peptide_mutation_position, intermediate_peptide_counters, peptide_sequence_info, peptide_info, mutation_info, p_length, reference_peptides):
    for normpep, mutpep, mutpos in izip(normpeps, mutpeps, peptide_mutation_position):
        if '*' in normpep or '*' in mutpep : # removing peptides from pseudogenes including a *
            intermediate_peptide_counters['peptide_removal_count'] += 1
            continue
        if 'X' in normpep or 'X' in mutpep:
            intermediate_peptide_counters['peptide_removal_count'] += 1
            continue
        if 'U' in normpep or 'U' in mutpep: # removing peptides with U - non normal AA 
            intermediate_peptide_counters['peptide_removal_count'] += 1
            continue
        if len(peptide_sequence_info.mutation_sequence) < p_length:
            continue
        if mutpep in reference_peptides: # Counting peptides matching a 100 % normal - Not removing 
            intermediate_peptide_counters['mutation_normal_match_count'] += 1
        intermediate_peptide_counters['mutation_peptide_count'] += 1

        pep_match_info = None # empty variable

        # fill dictionary
        if mutpep in peptide_info and normpep in peptide_info[mutpep]:
            # print '%s peptide already exist' % mutpep
            peptide_info[mutpep][normpep][0].append(mutation_info)
        else:
            peptide_info[mutpep][normpep] = [[mutation_info], peptide_sequence_info, mutpos, pep_match_info]

    return peptide_info, intermediate_peptide_counters


def mutation_sequence_creation(mutation_info, proteome_reference, genome_reference, peptide_length):
    # Create empty named tuple
    PeptideSequenceInfo = namedtuple('peptide_sequence_info', ['chop_normal_sequence', 'mutation_sequence', 'normal_sequence','mutation_position', 'consequence'])

    if mutation_info.mutation_consequence == 'missense_variant' :
        peptide_sequence_info = missense_variant_peptide(proteome_reference, mutation_info, peptide_length, PeptideSequenceInfo)
    elif mutation_info.mutation_consequence == 'inframe_insertion' :
        peptide_sequence_info = insertion_peptide(proteome_reference, mutation_info, peptide_length, PeptideSequenceInfo)
    elif mutation_info.mutation_consequence == 'inframe_deletion' :
        peptide_sequence_info = deletion_peptide(proteome_reference, mutation_info, peptide_length, PeptideSequenceInfo)
    elif mutation_info.mutation_consequence == 'frameshift_variant' :
        peptide_sequence_info = frame_shift_peptide(genome_reference, proteome_reference, mutation_info, peptide_length, PeptideSequenceInfo)
    else :
        peptide_sequence_info = None
    return peptide_sequence_info


def long_peptide_fasta_creation(peptide_sequence_info, mutation_info, fasta_printout):
    header = '>DTU|{trans1}:{pos1}/{trans2}:{pos2}|{predicted_effect}'.format(predicted_effect = mutation_info.predicted_effect,
        trans1 = mutation_info.trans1,
        pos1 = mutation_info.trans1_end_pos,
        trans2 = mutation_info.trans2,
        pos2 = mutation_info.trans2_start_pos)
    # print(header)
    seq = peptide_sequence_info.mutation_sequence
    fasta_printout[header] = seq

    return fasta_printout



def normal_peptide_identification(mutated_peptides_missing_normal, mutpeps, reference_peptides, mutation_info):
    for mutpep in mutpeps:
        mutated_peptides_missing_normal.add(mutpep)
    return mutated_peptides_missing_normal



def normal_peptide_correction(mutated_peptides_missing_normal, mutation_info, peptide_length, reference_peptide_file_names, peptide_info, peptide_match, tmp_dir, pepmatch_file_names, webserver, print_mismatch, num_mismatches):
    # write input file
    mutpeps_file = NamedTemporaryFile(delete = False, dir = tmp_dir)
    mutpeps_file.write('{}\n'.format('\n'.join(mutated_peptides_missing_normal)))
    mutpeps_file.close()

    pepmatch_file = run_peptide_match(mutpeps_file, peptide_length, peptide_match, reference_peptide_file_names, mutation_info, tmp_dir, webserver, print_mismatch, num_mismatches)
    pep_match = build_pepmatch(pepmatch_file, peptide_length, print_mismatch, peptide_info)
    pepmatch_file_names[peptide_length] = pepmatch_file 

    # insert normal in peptide info
    for mutated_peptide in pep_match:
        assert mutated_peptide in peptide_info
        for normal_peptide in peptide_info[mutated_peptide].keys():
            # renaming normal key, thereby inserting the normal peptide 
            peptide_info[mutated_peptide][pep_match[mutated_peptide].normal_peptide] = peptide_info[mutated_peptide].pop(normal_peptide)
            peptide_info[mutated_peptide][pep_match[mutated_peptide].normal_peptide][3] = pep_match[mutated_peptide] 

    return peptide_info, pepmatch_file_names


def run_peptide_match(mutpeps_file, peptide_length, peptide_match, reference_peptide_file_names, mutation_info, tmp_dir, webserver, print_mismatch, num_mismatches):
    reference_peptide_file = reference_peptide_file_names[peptide_length]
    print_ifnot_webserver('\t\tRunning {} aa normal peptide match'.format(peptide_length), webserver)
    reference_peptide_file_name = reference_peptide_file.name if not type(reference_peptide_file) == str else reference_peptide_file
    pepmatch_file = NamedTemporaryFile(delete = False, dir = tmp_dir)
    if not print_mismatch == None:
        process_pepmatch = subprocess.Popen([peptide_match, '-mm', '-thr' , str(num_mismatches), mutpeps_file.name, reference_peptide_file_name], stdout = pepmatch_file)
    else:
        process_pepmatch = subprocess.Popen([peptide_match, '-thr' , str(num_mismatches), mutpeps_file.name, reference_peptide_file_name], stdout = pepmatch_file)
    process_pepmatch.communicate() # now wait
    pepmatch_file.close()
    return pepmatch_file



def build_pepmatch(pepmatch_file, peptide_length, print_mismatch, peptide_info):
    pep_match = defaultdict(dict) # empty dictionary

    with open(pepmatch_file.name) as f:
        for line in f.readlines():
            if re.search(r'^Hit\s', line):
                line = [x.strip() for x in line.split()]
                # save information
                mutated_peptide = line[2]
                normal_peptide = line[3]
                mismatch_peptide = line[4] if not print_mismatch == None else '-' * len(normal_peptide)
                mismatch = line[5] if print_mismatch == None else line[6]
                # not interested in peptides with no mismatch:
                if mismatch == '0':
                    print('%s with mismatch 0, removing from peptide list.' % mutated_peptide)
                    del peptide_info[mutated_peptide]
                    continue
            elif re.search(r'^No Hit found\s', line):
                line = [x.strip() for x in line.split()]
                mutated_peptide = line[3]
                normal_peptide = '-' * len(mutated_peptide)
                mismatch_peptide = '-' * len(mutated_peptide)
                mismatch = 5
            else:
                continue
            # create named tuple 
            PepMatchInfo = namedtuple('pep_match_info', ['normal_peptide', 'mismatch', 'mismatch_peptide'])
            pep_match_info = PepMatchInfo(normal_peptide, int(mismatch), mismatch_peptide)
            # fill dictionary
            pep_match[mutated_peptide] = pep_match_info
    return pep_match



def missense_variant_peptide(proteome_reference, mutation_info, peptide_length, PeptideSequenceInfo):
    asserted_proteome = reference_assertion(proteome_reference, mutation_info, reference_type = 'proteome')
    aaseq = asserted_proteome[0]
    index = index_creator(mutation_info.prot_pos, peptide_length, aaseq, cdna_pos_start = None, index_type = 'amino_acid', codon = None, frame_type = None)
    normal_sequence = aaseq[index.lower_index:index.higher_index]
    mutation_sequence = normal_sequence[ :index.mutation_peptide_position - 1] + mutation_info.aa_mut + normal_sequence[index.mutation_peptide_position: ]
    consequence = 'M'

    # Return long peptide (created) and information
    return PeptideSequenceInfo(normal_sequence, mutation_sequence, normal_sequence, index.mutation_peptide_position, consequence)



def insertion_peptide(proteome_reference, mutation_info, peptide_length, PeptideSequenceInfo):
    asserted_proteome = reference_assertion(proteome_reference, mutation_info, reference_type = 'proteome')
    aaseq = asserted_proteome[0]
    index = index_creator(mutation_info.prot_pos, peptide_length, aaseq, cdna_pos_start = None, index_type = 'amino_acid', codon = None, frame_type = None)
    normal_sequence = aaseq[index.lower_index:index.higher_index]

    if mutation_info.aa_normal == '-':
        mutation_sequence = normal_sequence[ :index.mutation_peptide_position] + mutation_info.aa_mut + normal_sequence[index.mutation_peptide_position: ]
    else:
        mutation_sequence = normal_sequence[ :index.mutation_peptide_position - 1] + mutation_info.aa_mut + normal_sequence[index.mutation_peptide_position: ]

    chop_normal_sequence = '-' * len(mutation_sequence)
    insertion_range = '{}:{}'.format(index.mutation_peptide_position, index.mutation_peptide_position + len(mutation_info.aa_mut))
    consequence = 'I'

    # Return long peptide (created) and information
    return PeptideSequenceInfo(chop_normal_sequence, mutation_sequence, normal_sequence, insertion_range, consequence)



def deletion_peptide(proteome_reference, mutation_info, peptide_length, PeptideSequenceInfo):
    asserted_proteome = reference_assertion(proteome_reference, mutation_info, reference_type = 'proteome')
    aaseq = asserted_proteome[0]
    index = index_creator(mutation_info.prot_pos, peptide_length, aaseq, cdna_pos_start = None, index_type = 'amino_acid', codon = None, frame_type = None)
    normal_sequence = aaseq[index.lower_index:index.higher_index]

    if mutation_info.aa_mut == '-':
        mutation_sequence = normal_sequence[ :index.mutation_peptide_position - 1] + normal_sequence[index.mutation_peptide_position: ]
    else:
        mutation_sequence = normal_sequence[ :index.mutation_peptide_position - 1] + mutation_info.aa_mut + normal_sequence[index.mutation_peptide_position + len(mutation_info.aa_mut): ]

    chop_normal_sequence = '-'*len(mutation_sequence)
    consequence = 'D'

    # Return long peptide (created) and information
    return PeptideSequenceInfo(chop_normal_sequence, mutation_sequence, normal_sequence, index.mutation_peptide_position - 1, consequence)



def frame_shift_peptide(genome_reference, proteome_reference, mutation_info, peptide_length, PeptideSequenceInfo):
    asserted_genome = reference_assertion(genome_reference, mutation_info, reference_type = 'genome')
    asserted_proteome = reference_assertion(proteome_reference, mutation_info, reference_type = 'proteome')
    seq = asserted_genome[0]
    cdna_pos_start = asserted_genome[1]
    cdna_pos_end = asserted_genome[2]
    aaseq = asserted_proteome[0]

    aa_index = index_creator(mutation_info.prot_pos, peptide_length, aaseq, cdna_pos_start = None, index_type = 'amino_acid', codon = None, frame_type = None)
    normal_sequence = aaseq[aa_index.lower_index:aa_index.higher_index]

    if mutation_info.codon_mut.islower() : # frame shift deletion
        n_index = index_creator(mutation_info.prot_pos, peptide_length, aaseq, mutation_info.codon_normal, cdna_pos_start, index_type = 'nucleotide', frame_type = None)
        mutation_sequence = seq[n_index.lower_index :n_index.cdna_codon_position] + mutation_info.codon_mut.lower() + seq[n_index.cdna_codon_position + 3: ]
    else: # frame shift insertion
        n_index = index_creator(mutation_info.prot_pos, peptide_length, aaseq, mutation_info.codon_mut, cdna_pos_start, index_type = 'nucleotide', frame_type = 'frameshift_insertion')
        new_codon = re.search(r'([A-Z]+)', mutation_info.codon_mut).group(1).strip()
        if mutation_info.codon_normal == '-':
            mutation_sequence = seq[n_index.lower_index - 3:cdna_pos_start + 1] + new_codon.lower() + seq[cdna_pos_end : ]
        else:
            mutation_sequence = seq[n_index.lower_index :cdna_pos_start] + new_codon.lower() + seq[cdna_pos_end - 1: ]

    # BIO PYTHON:
    # ignore biopython warnings (as we intentionally create sequences not multiple by three) 
    with warnings.catch_warnings():
        warnings.simplefilter('ignore', BiopythonWarning)
        # When mutation sequence is obtained, translate the sequence using biopython 
        dna_sequence = Seq(mutation_sequence, generic_dna)
        mutation_aaseq = str(dna_sequence.translate(to_stop = True))

    chop_normal_sequence = '-'*len(mutation_aaseq)
    consequence = 'F'
    frameshift_range = '{}:{}'.format(aa_index.mutation_peptide_position, len(mutation_aaseq))

    # Return long peptides and information
    return PeptideSequenceInfo(chop_normal_sequence, mutation_aaseq, normal_sequence, frameshift_range, consequence)


def fusion_sequneces_creation(genome_reference, cds_start_db, fusion_info, peptide_length):
    PeptideSequenceInfo = namedtuple('peptide_sequence_info', ['chop_normal_sequence', 'mutation_sequence', 'normal_sequence', 'mutation_position', 'consequence', 'mut_protein_position'])
    # get transcripts seqs:
    seq1 = genome_reference[fusion_info.gene1][fusion_info.trans1]
    seq2 = genome_reference[fusion_info.gene2][fusion_info.trans2]

    # find start codons:
    cds_start1 = cds_start_db[fusion_info.gene1][fusion_info.trans1]['relative_start']
    cds_start2 = cds_start_db[fusion_info.gene2][fusion_info.trans2]['relative_start']
    orientation1 = cds_start_db[fusion_info.gene1][fusion_info.trans1]['orientation']
    orientation2 = cds_start_db[fusion_info.gene2][fusion_info.trans2]['orientation']
    assert cds_start2 < fusion_info.trans2_start_pos

    # trim seq1 to start from ATG
    seq1 = seq1[cds_start1:]
    fused_seq = seq1[:fusion_info.trans1_end_pos-cds_start1] + seq2[fusion_info.trans2_start_pos - 1:]

    # BIO PYTHON:
    # ignore biopython warnings (as we intentionally create sequences not multiple by three)
    with warnings.catch_warnings():
        warnings.simplefilter('ignore', BiopythonWarning)
        # When mutation sequence is obtained, translate the sequence using biopython
        dna_sequence = Seq(fused_seq, generic_dna)
        whole_mutation_aaseq = str(dna_sequence.translate(to_stop=True))


    assert whole_mutation_aaseq == fusion_info.fused_protein, 'generated fused protein doesn\'t match input fused protein\n%s\n%s\n' % (whole_mutation_aaseq, fusion_info.fused_protein)

    # getting mutation range:
    protein_position = int(math.floor((fusion_info.trans1_end_pos - cds_start1 - 1) / 3) + 1)
    # print(protein_position)
    lower_index = max(protein_position - peptide_length, 0)
    upper_index = min(protein_position + peptide_length, len(whole_mutation_aaseq))
    fuse_peptide_position = protein_position - lower_index

    if fusion_info.predicted_effect == 'in-frame':
        mutation_aaseq = whole_mutation_aaseq[lower_index:upper_index]
        frameshift_range = '{}:{}'.format(fuse_peptide_position, upper_index)
    elif fusion_info.predicted_effect == 'out-of-frame':
        mutation_aaseq = whole_mutation_aaseq[lower_index:]
        frameshift_range = '{}:{}'.format(fuse_peptide_position, len(whole_mutation_aaseq))
    else:
        raise Exception('Unknown predicted effect: %s' % fusion_info.predicted_effect)

    normal_sequence = None
    chop_normal_sequence = '-'*len(mutation_aaseq)
    consequence = 'F'

    # # Return long peptides and information
    if len(mutation_aaseq) < peptide_length:
        return None
    else:
        return PeptideSequenceInfo(chop_normal_sequence, mutation_aaseq, normal_sequence, frameshift_range, consequence, protein_position)


def find_codon_position(codon, frame_type):
    for i in range(len(codon)):
        if codon[i].isupper():
            break
    if frame_type == 'frameshift_insertion':
        i -= 1
    return i


def index_creator(protein_position, peptide_length, aaseq, codon, cdna_pos_start, index_type, frame_type) :
    if index_type == 'amino_acid':
        lower_index = max(protein_position - peptide_length, 0)
        higher_index = min(protein_position + (peptide_length - 1), len(aaseq))
        mutation_peptide_position = protein_position - lower_index
        Index = namedtuple('index', ['lower_index', 'higher_index', 'mutation_peptide_position'])
        index = Index(lower_index, higher_index, mutation_peptide_position)
    elif index_type == 'nucleotide':
        codon_position = find_codon_position(codon, frame_type)
        cdna_codon_position = (cdna_pos_start - 1) - codon_position
        cda_transcription_start_position = cdna_codon_position - ((protein_position - 1) * 3)
        lower_index = max(cdna_codon_position - (peptide_length * 3 - 3), cda_transcription_start_position)
        Index = namedtuple('index', ['lower_index', 'cdna_codon_position'])
        index = Index(lower_index, cdna_codon_position)
    return index



def reference_assertion(reference, mutation_info, reference_type):
    # Check gene id and transcript id exists in proteome_reference dictionary 
    assert mutation_info.gene_id in reference, '{gene_id}: This gene is not in the reference gene set. Check that you have used the right reference corresponding to the one used when running VEP'.format(trans_id = mutation_info.gene_id)
    assert mutation_info.trans_id in reference[mutation_info.gene_id], '{trans_id}: This transcript is not in the reference gene set. Check that you have used the right reference corresponding to the one used when running VEP'.format(trans_id = mutation_info.trans_id)
    seq = reference[mutation_info.gene_id][mutation_info.trans_id]
    asserted_output = [seq]

    if reference_type == 'proteome':
        # Check mutation is within length of amino acid sequence
        assert len(seq) >= mutation_info.prot_pos, 'amino acid sequence length ({}) less than mutation position {}'.format(len(seq), mutation_info.prot_pos)

    elif reference_type == 'genome':
        # Check mutation site is within length of cDNA sequence
        cdna_pos_start = int(mutation_info.cdna_pos.split('-')[0])
        cdna_pos_end = int(mutation_info.cdna_pos.split('-')[1]) if '-' in mutation_info.cdna_pos else cdna_pos_start
        assert len(seq) >= cdna_pos_start, 'cDNA sequence length ({}) less than mutation position start {}'.format(len(seq), cdna_pos_start)
        assert len(seq) >= cdna_pos_end, 'cDNA sequence length ({}) less than mutation position end {}'.format(len(seq), cdna_pos_end)
        asserted_output.extend([cdna_pos_start, cdna_pos_end])

    return asserted_output


def write_fasta (tmp_dir, fasta_printout, webserver):
    if not fasta_printout == None:
        print_ifnot_webserver('\tWriting Fasta file', webserver)
        fasta_file = NamedTemporaryFile(delete = False, dir = tmp_dir)
        print(fasta_file.name)
        for header in fasta_printout:
                fasta_file.write('{Header}\n{Sequence}\n'.format(Header = header, Sequence = fasta_printout[header]))
        fasta_file.close()
    else:
        fasta_file = None
    return fasta_file





"""

FuPeXI

"""



def write_peptide_file(peptide_info, tmp_dir, webserver, keep_tmp, file_prefix, outdir):
    print_ifnot_webserver('\tWriting temporary peptide file', webserver)

    unique_mutant_peptide_count = 0
    peptide_file_names = defaultdict(dict) # empty dictionary
    peptides = set()

    for mutant_petide in peptide_info:
        unique_mutant_peptide_count += 1
        peptides.add(mutant_petide) 
        for normal_peptide in peptide_info[mutant_petide]:
            peptides.add(normal_peptide)

    # write temporary peptide file 
    peptide_file = NamedTemporaryFile(delete = False, dir = tmp_dir)
    peptide_file.write('{}\n'.format('\n'.join(peptides)))
    peptide_file.close()

    keep_temp_file(keep_tmp, 'txt', peptide_file.name, file_prefix, outdir, None, 'peptide_netMHCinput')

    return unique_mutant_peptide_count, peptide_file



def run_netMHCpan(HLA_alleles, netMHCpan3_0, peptide_file, tmp_dir, webserver, keep_tmp, file_prefix, outdir):
    # isolate unique HLAalleles 
    unique_alleles_set = set(HLA_alleles.split(','))
    unique_alleles = ','.join(map(str, unique_alleles_set))

    print_ifnot_webserver('\tRunning NetMHCpan', webserver)
    netMHCpan_start = datetime.now()
    netMHC_file = NamedTemporaryFile(delete = False, dir = tmp_dir)
    process_netMHC = subprocess.Popen([netMHCpan3_0, '-p', '-a', unique_alleles, '-f', peptide_file.name], stdout = netMHC_file)
    process_netMHC.communicate() # now wait
    netMHC_file.close()

    keep_temp_file(keep_tmp, 'txt', netMHC_file.name, file_prefix, outdir, None, 'netMHCpan')

    netMHCpan_runtime = datetime.now() - netMHCpan_start
    return netMHCpan_runtime, unique_alleles, netMHC_file



def build_netMHC(netMHC_file, webserver):
    print_ifnot_webserver ('\tCreating NetMHCpan file dictionary', webserver)
    net_mhc = defaultdict(dict) # empty dictionary
    NetMHCInfo = namedtuple('NetMHCInfo', ['affinity', 'rank'])

    with open(netMHC_file.name) as f:
        for line in f.readlines():
            if re.search(r'^\s+1\s', line):
                line = [x.strip() for x in line.split()]
                if 'PEPLIST' in line[10]:
                    line[2] = '-' * len(line[2]) if line[2].startswith('XXXX') else line[2]
                    # save information
                    HLA_allele = line[1].replace('*','')
                    peptide = line[2]
                    affinity = float(line[12])
                    rank = float(line[13])
                    # fill tuple
                    netmhc_info = NetMHCInfo(affinity, rank)
                    # fill dictionary
                    net_mhc[HLA_allele][peptide] = netmhc_info

    return net_mhc



def write_output_file_fork(peptide_info, net_mhc, unique_alleles, tmp_dir, webserver, print_mismatch, reference_peptides, proteome_reference, version):
    print_ifnot_webserver('\tWriting output file', webserver)
    printed_ids = set()
    row = 0

    # Create data frame
    df = pandas.DataFrame(columns = (
        'HLA_allele',
        'Norm_peptide',
        'Norm_MHCAffinity',
        'Norm_MHCrank',
        'Mut_peptide',
        'Mut_MHCAffinity',
        'Mut_MHCrank',

        'Fusion_protein_position',
        'Fusion_peptide_position',
        'peptide_position_on_protein',
        'Mismatches',

        'Peptide_count',
        'Fusion_input_row',
        'Gene_1_symbol',
        'Gene_2_symbol',
        'Fusion_description',
        'Counts_of_common_mapping_reads',
        'Spanning_pairs',
        'Spanning_unique_reads',
        'Longest_anchor_found',
        'Fusion_finding_method',
        'Fusion_point_for_gene_1',
        'Fusion_point_for_gene_2',
        'Gene_1_id',
        'Gene_2_id',
        'Exon_1_id',
        'Exon_2_id',
        'Fusion_sequence',
        'Predicted_effect',
        'Predicted_fused_transcripts'
        # 'Predicted_fused_proteins'
        ), )

    # Extract data
    for mutant_petide in peptide_info :
        for normal_peptide in peptide_info[mutant_petide]:
            for hla in unique_alleles.split(','):
                # Checking concordance between MHC file and intermediate peptide_info file
                assert hla in net_mhc
                assert mutant_petide in net_mhc[hla]
                assert normal_peptide in net_mhc[hla]
                # save information tuples
                mutation_infos = peptide_info[mutant_petide][normal_peptide][0]
                peptide_sequence_info = peptide_info[mutant_petide][normal_peptide][1]
                mutant_netmhc_info = net_mhc[hla][mutant_petide]
                normal_netmhc_info = net_mhc[hla][normal_peptide]
                pep_match_info = peptide_info[mutant_petide][normal_peptide][3]

                fusion_peptide_position = int(peptide_info[mutant_petide][normal_peptide][2].split(':')[0])
                fusion_protein_position = peptide_sequence_info.mut_protein_position
                peptide_position_on_protein = fusion_protein_position - fusion_peptide_position + 1

                # Extract mismatches
                mismatches = pep_match_info.mismatch if not pep_match_info == None else 1
                # Print normal peptide or normal peptide only showing mis matched (...X...XX...)
                print_normal_peptide = mismatch_snv_normal_peptide_conversion(normal_peptide, fusion_peptide_position, peptide_sequence_info.consequence, pep_match_info) if not print_mismatch == None else normal_peptide

                # arrange input data:
                gene1_symbol_list = [mutation_info.gene1_symbol for mutation_info in mutation_infos]
                gene2_symbol_list = [mutation_info.gene2_symbol for mutation_info in mutation_infos]
                fuse_desc_list = [mutation_info.fuse_desc for mutation_info in mutation_infos]
                common_mapping_reads_count_list = [mutation_info.common_mapping_reads_count for mutation_info in mutation_infos]
                spanning_pairs_list = [mutation_info.spanning_pairs for mutation_info in mutation_infos]
                spanning_unique_pairs_list = [mutation_info.spanning_unique_pairs for mutation_info in mutation_infos]
                longest_anchor_found_list = [mutation_info.longest_anchor_found for mutation_info in mutation_infos]
                finding_method_list = [mutation_info.finding_method for mutation_info in mutation_infos]
                fusion_point1_list = [mutation_info.fusion_point1 for mutation_info in mutation_infos]
                fusion_point2_list = [mutation_info.fusion_point2 for mutation_info in mutation_infos]
                gene1_list = [mutation_info.gene1 for mutation_info in mutation_infos]
                gene2_list = [mutation_info.gene2 for mutation_info in mutation_infos]
                exon1_list = [mutation_info.exon1 for mutation_info in mutation_infos]
                exon2_list = [mutation_info.exon2 for mutation_info in mutation_infos]
                fusion_sequence_list = [mutation_info.fusion_sequence for mutation_info in mutation_infos]
                predicted_effect_list = [mutation_info.predicted_effect for mutation_info in mutation_infos]
                predicted_fused_transcript_list = [mutation_info.predicted_fused_transcript for mutation_info in mutation_infos]
                fused_protein_list = [mutation_info.fused_protein for mutation_info in mutation_infos]

                # Add row to data frame
                df.loc[row] = [
                hla,
                print_normal_peptide,
                normal_netmhc_info.affinity,
                normal_netmhc_info.rank,
                mutant_petide,
                mutant_netmhc_info.affinity,
                mutant_netmhc_info.rank,

                fusion_protein_position,
                fusion_peptide_position,
                peptide_position_on_protein,
                mismatches,

                len(gene1_symbol_list),
                mutation_info.row_idx,
                ','.join(gene1_symbol_list) if len(set(gene1_symbol_list)) > 1 else gene1_symbol_list[0],
                ','.join(gene2_symbol_list) if len(set(gene2_symbol_list)) > 1 else gene2_symbol_list[0],
                ','.join(fuse_desc_list) if len(set(fuse_desc_list)) > 1 else fuse_desc_list[0],
                ','.join(common_mapping_reads_count_list) if len(set(common_mapping_reads_count_list)) > 1 else common_mapping_reads_count_list[0],
                ','.join(spanning_pairs_list) if len(set(spanning_pairs_list)) > 1 else spanning_pairs_list[0],
                ','.join(spanning_unique_pairs_list) if len(set(spanning_unique_pairs_list)) > 1 else spanning_unique_pairs_list[0],
                ','.join(longest_anchor_found_list) if len(set(longest_anchor_found_list)) > 1 else longest_anchor_found_list[0],
                ','.join(finding_method_list) if len(set(finding_method_list)) > 1 else finding_method_list[0],
                ','.join(fusion_point1_list) if len(set(fusion_point1_list)) > 1 else fusion_point1_list[0],
                ','.join(fusion_point2_list) if len(set(fusion_point2_list)) > 1 else fusion_point2_list[0],
                ','.join(gene1_list) if len(set(gene1_list)) > 1 else gene1_list[0],
                ','.join(gene2_list) if len(set(gene2_list)) > 1 else gene2_list[0],
                ','.join(exon1_list) if len(set(exon1_list)) > 1 else exon1_list[0],
                ','.join(exon2_list) if len(set(exon2_list)) > 1 else exon2_list[0],
                ','.join(fusion_sequence_list) if len(set(fusion_sequence_list)) > 1 else fusion_sequence_list[0],
                ','.join(predicted_effect_list) if len(set(predicted_effect_list)) > 1 else predicted_effect_list[0],
                ','.join(predicted_fused_transcript_list)
                # ','.join(fused_protein_list)
                ]

                row += 1

    # Sort, round up prioritization score
    print_ifnot_webserver('\tSorting output file', webserver)
    df_sorted = df.sort_values('Mut_MHCAffinity', ascending=False) if not pandas.__version__ == '0.16.0' else df.sort(columns = ('Mut_MHCAffinity'), ascending=False)
    df_sorted.loc[:,'Mismatches'] = df_sorted.Mismatches.astype(int)
    df_sorted.loc[:, 'Fusion_protein_position'] = df_sorted.Fusion_protein_position.astype(int)
    df_sorted.loc[:, 'Fusion_peptide_position'] = df_sorted.Fusion_peptide_position.astype(int)
    df_sorted.loc[:, 'peptide_position_on_protein'] = df_sorted.peptide_position_on_protein.astype(int)
    df_sorted.loc[:, 'Peptide_count'] = df_sorted.Peptide_count.astype(int)
    df_sorted.loc[:, 'Fusion_input_row'] = df_sorted.Fusion_input_row.astype(int)


    # Print data frame to intermediate file
    df_file = NamedTemporaryFile(delete = False, dir = tmp_dir)
    df_sorted.to_csv(df_file.name , sep = '\t', index = False)
    df_file.close()

    # Print header to output file
    header_file = NamedTemporaryFile(delete = False, dir = tmp_dir)
    header = "# VERSION:\tFuPeXI {version}\n# CALL:\t\t{call}\n# DATE:\t\t{day} {date} of {month} {year}\n# TIME:\t\t{print_time}\n# PWD:\t\t{pwd}\n"
    header_file.write(header.format(version = version,
        call = ' '.join(map(str, sys.argv)),
        day = datetime.now().strftime("%A"),
        month = datetime.now().strftime("%B"),
        year = datetime.now().strftime("%Y"),
        date = datetime.now().strftime("%d"),
        print_time = datetime.now().strftime("%T"),
        pwd = os.getcwd()))
    header_file.close()

    # combining header and data frame file
    output_file = NamedTemporaryFile(delete = False, dir = tmp_dir)
    process_cat_files = subprocess.Popen(['cat', header_file.name, df_file.name], stdout = output_file)
    process_cat_files.communicate()
    output_file.close()

    return output_file



def mismatch_snv_normal_peptide_conversion(normal_peptide, peptide_position, consequence, pep_match_info):
    if consequence == 'M':
        dot_line = '.' * len(normal_peptide)
        normal_mismatch_peptide = dot_line[0:peptide_position - 1] + normal_peptide[peptide_position - 1] + dot_line[peptide_position:]
    else:
        normal_mismatch_peptide = pep_match_info.mismatch_peptide
    return normal_mismatch_peptide


def write_log_file_fork(argv, peptide_length, sequence_count, reference_peptide_counters, vep_counters, peptide_counters, start_time_mupex, start_time_mupei, start_time, end_time_mupex, HLAalleles, netMHCpan_runtime, unique_mutant_peptide_count, unique_alleles, tmp_dir, webserver, version):
    print_ifnot_webserver('\tWriting log file\n', webserver)
    log_file = NamedTemporaryFile(delete = False, dir = tmp_dir)

    log = """
        # VERSION:  FuPeXI {version}
        # CALL:     {call}
        # DATE:     {day} {date} of {month} {year}
        # TIME:     {print_time}
        # PWD:      {pwd}

        ----------------------------------------------------------------------------------------------------------
                                                        MuPeX
        ----------------------------------------------------------------------------------------------------------

          Reading protein reference file:            Found {sequence_count} sequences, with {reference_peptide_count} {peptide_length}mers
                                                           of which {unique_reference_peptide_count} were unique peptides
          Checking peptides:                         {normal_match_count} peptides matched a normal peptide. 
                                                     {removal_count} peptides included unsupported symbols (e.g. *, X, U) and were discarded 

          Final Result:                              {peptide_count} potential mutant peptides
          MuPeX Runtime:                             {time_mupex}

        ----------------------------------------------------------------------------------------------------------
                                                        MuPeI
        ----------------------------------------------------------------------------------------------------------

          Reading through FuPexi file:                Found {peptide_count} peptides of which {unique_mutant_peptide_count} were unique
          Detecting HLA alleles:                     Detected the following {num_of_HLAalleles} HLA alleles:
                                                        {HLAalleles}
                                                        of which {num_of_unique_alleles} were unique
          Running NetMHCpan 3.0:                     Analyzed {num_of_unique_alleles} HLA allele(s)
                                                     NetMHCpan runtime: {netMHCpan_runtime}

          MuPeI Runtime:                             {time_mupei}

          TOTAL Runtime:                             {time}
          """
    log_file.write(log.format(version = version,
        call = ' '.join(map(str, argv)),
        sequence_count = sequence_count,
        reference_peptide_count = reference_peptide_counters.total_peptide_count,
        unique_reference_peptide_count = reference_peptide_counters.unique_peptide_count,
        peptide_length = peptide_length,
        normal_match_count = peptide_counters.normal_match_count,
        removal_count = peptide_counters.removal_count,
        peptide_count = peptide_counters.peptide_count,
        time_mupex = end_time_mupex - start_time_mupex,
        unique_mutant_peptide_count = unique_mutant_peptide_count,
        num_of_HLAalleles = len(HLAalleles.split(',')),
        num_of_unique_alleles = len(unique_alleles.split(',')),
        HLAalleles = HLAalleles,
        netMHCpan_runtime = netMHCpan_runtime,
        time_mupei = datetime.now() - start_time_mupei,
        time = datetime.now() - start_time,
        day = datetime.now().strftime("%A"),
        month = datetime.now().strftime("%B"),
        year = datetime.now().strftime("%Y"),
        date = datetime.now().strftime("%d"),
        print_time = datetime.now().strftime("%T"),
        pwd = os.getcwd()
        ))
    log_file.close()
    return log_file



def move_output_files(outdir, log_file, logfile_name, fasta_file, fasta_file_name, output_file, output_file_name, webserver, www_tmp_dir):
    # moving output files to user defined dir or webserver dir  
    move_to_dir = outdir if webserver == None else www_tmp_dir

    shutil.move(log_file.name, '{}/{}'.format(move_to_dir, logfile_name))
    shutil.move(output_file.name, '{}/{}'.format(move_to_dir, output_file_name))
    if not fasta_file_name == None:
        shutil.move(fasta_file.name, '{}/{}'.format(move_to_dir, fasta_file_name))

    if not webserver == None :
        os.system('chmod -R a+rwx {}'.format(www_tmp_dir))



def keep_temp_file(keep_temp, file_extension, file_name, file_prefix, move_to_dir, peptide_lengths, filetype):
    if not keep_temp == None:
        if peptide_lengths == None:
            shutil.copy(file_name, '{}/{}_{}.{}'.format(move_to_dir, file_prefix, filetype, file_extension))
        else:
            for p_length in peptide_lengths:
                file_ = file_name[p_length]
                if not type(file_) == str:
                    shutil.copy(file_.name, '{}/{}_{}_{}.{}'.format(move_to_dir, file_prefix, filetype, p_length, file_extension))
                else:
                    continue





def clean_up(tmp_dir):
    shutil.rmtree(tmp_dir)



def webserver_print_output(webserver, www_tmp_dir, output, logfile, fasta_file_name):
    # If webserver copy log and output files to www_tmp_dir
    if not webserver == None :
       dir_name = os.path.basename(os.path.normpath(www_tmp_dir))

       os.system('cat {}/{}'.format(www_tmp_dir,logfile)) 

       print '\n-------------------------------------------------------------\n'

       print '\nLink to FuPeXI output file <a href="/services/FuPeXI-1.1/tmp/{}/{}">FuPeXI.out</a>\n'.format(dir_name, output)
       print 'Link to FuPeXI log file <a href="/services/FuPeXI-1.1/tmp/{}/{}">FuPeXI.log</a>\n'.format(dir_name, logfile)
       if not fasta_file_name == None:
          print 'Link to Fasta file with peptide info <a href="/services/FuPeXI-1.1/tmp/{}/{}">Fasta file</a>\n'.format(dir_name, fasta_file_name)


def usage():
    usage =   """
        FuPeXI - Mutant Peptide Extractor and Informer

        The current version of this program is available from
        https://bitbucket.org/incpm/fupexi

        FuPeXI.py accepts the output of FusionCatcher, and from this 
        derives a set of mutated peptides of specified length(s). These mutated peptides 
        are returned in a table along with various annotations that may be useful for 
        predicting the immunogenicity of each peptide.

        Usage: {call} -i <FusionCatcher-file> [options]

                                                                                    DEFAULT
        Required arguments:
        -i                      the output file of FusionCatcher

        Recommended arguments:
        -a, --alleles           HLA alleles, comma separated.                       HLA-A02:01
        -l, --length            Peptide length, given as single number,             9
                                range (9-11) or comma separated (9,10,11).

        Optional arguments affecting output files:
        -o, --output-file       Output file name.                                   <input-file>.fupexi
        -d, --out-dir           Output directory - full path.                       current directory
        -p, --prefix            Prefix for output files - will be applied           <input-file>
                                to all (.fupexi, .log, .fasta) unless specified 
                                by -o or -L.
        -L, --log-file          Logfile name.                                       <input-file>.log
        -m, --mismatch-number   Maximum number of mismatches to search for in       4
                                normal peptide match.

        Other options (these do not take values)
        -f, --make-fasta        Create FASTA file with long peptides 
                                - mutation in the middle
        -c, --config-file       Path to the config.ini file                         current directory
        -t, --keep-temp         Retain all temporary files
        -M, --mismatch-only     Print only mismatches in normal peptide sequence 
                                and otherwise use dots (...AA.....)
        -w, --webface           Run in webserver mode
        -h, --help              Print this help information

        REMEMBER to state references in the config.ini file

        """
    print(usage.format(call = sys.argv[0], path = '/'.join(sys.argv[0].split('/')[0:-1]) ))



def read_options(argv):
    try:
        optlist, args = getopt.getopt(argv,
            'i:a:l:o:d:L:c:p:m:ftMh',
            ['input-file=', 'alleles=', 'length=', 'output-file=', 'out-dir=', 'log-file=', 'config-file=', 'prefix=', 'mismatch-number=', 'make-fasta', 'keep-temp', 'mismatch-print', 'help'])
        if not optlist:
            print 'No options supplied'
            usage()
    except getopt.GetoptError, e:
        usage(); sys.exit(e)

    # Create dictionary of long and short formats 
    format_dict = {
        '-h': '--help',
        '-i': '--fusion-file',
        '-l': '--length',
        '-p': '--prefix',
        '-o': '--output-file',
        '-d': '--out-dir',
        '-L': '--log-file',
        '-c': '--config-file',
        '-f': '--make-fasta',
        '-t': '--keep-temp',
        '-a': '--alleles',
        '-m': '--mismatch-number',
        '-M': '--mismatch-only'
    }

    # Create a dictionary of options and input from the options list
    opts = dict(optlist)

    # Use the long format dictionary to change the option to the short annotation, if long is given by the user.
    for short, long_ in format_dict.iteritems():
        if long_ in opts:
            opts[short] = opts.pop(long_)

    # Print usage help 
    if '-h' in opts.keys():
        usage(); sys.exit()

    # Define values 
    fusion_file = opts['-i'] if '-i' in opts.keys() else None
    if fusion_file == None :
        usage(); sys.exit('FusionCatcher file is missing!')
    peptide_length = opts['-l'] if '-l' in opts.keys() else 9
    if peptide_length < 0:
        usage(); sys.exit('Length of peptides should be positive!')
    webserver =  None
    prefix = opts['-p'] if '-p' in opts.keys() else fusion_file.split('/')[-1].split('.')[0]
    output = opts['-o'] if '-o' in opts.keys() else prefix + '.fupexi'
    outdir = opts['-d'] if '-d' in opts.keys() else os.getcwd()
    logfile = opts['-L'] if '-L' in opts.keys() else prefix + '.log'
    config = opts['-c'] if '-c' in opts.keys() else '/'.join(sys.argv[0].split('/')[0:-1]) +'/config.ini'
    fasta_file_name = prefix + '.fasta' if '-f' in opts.keys() else None
    keep_temp = 'yes' if '-t' in opts.keys() else None
    HLA_alleles = opts['-a'] if '-a' in opts.keys() else 'HLA-A02:01'
    print_mismatch = 'Yes' if '-M' in opts.keys() else None
    num_mismatches = opts['-m'] if '-m' in opts.keys() else 4
    species = 'human'
    # Create and fill input named-tuple
    Input = namedtuple('input', ['fusion_file', 'peptide_length', 'output', 'logfile', 'HLA_alleles', 'config', 'fasta_file_name', 'webserver', 'outdir', 'keep_temp', 'prefix', 'print_mismatch', 'num_mismatches', 'species'])
    inputinfo = Input(fusion_file, peptide_length, output, logfile, HLA_alleles, config, fasta_file_name, webserver, outdir, keep_temp, prefix, print_mismatch, num_mismatches, species)

    return inputinfo


################
# Run analysis #
################

if __name__ == '__main__':
    main(sys.argv[1:])
