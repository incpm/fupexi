#!/usr/bin/env python
import argparse
import csv
import re


def get_options():
    parser = argparse.ArgumentParser(description='This script create a file containing cds points relative to transcript start of all transcripts.')
    parser.add_argument('--gtf-file', required=True, help='gtf file')
    parser.add_argument('--output-file', required=True, help='output cds file')
    args = parser.parse_args()
    return args


DESC_PATTERN = re.compile('gene_id "([^"]+)".+transcript_id "([^"]+)"')


def create_cds_file(gtf_file, output_file):
    db_dict = {}
    print('reading gtf')
    with open(gtf_file, 'r') as gtf_fd:
        reader = csv.reader(gtf_fd, delimiter='\t')
        for row in reader:
            # ignore comment line:
            if row[0].startswith('#'):
                continue
            # ignore non protein coding lines:
            if '"protein_coding"' not in row[-1]:
                continue
            rec_type = row[2]
            desc = row[-1]
            orientation = row[6]
            # we only need exon and strat codon records:
            if rec_type != 'exon' and rec_type != 'start_codon':
                continue
            # extracting gene id and trans id:
            m = DESC_PATTERN.search(desc)
            gene_id = m.group(1)
            trans_id = m.group(2)
            # create dicts in needed:
            if gene_id not in db_dict:
                db_dict[gene_id] = {}
            if trans_id not in db_dict[gene_id]:
                db_dict[gene_id][trans_id] = {}
            # fill dict with relevant data:
            db_dict[gene_id][trans_id]['orientation'] = orientation
            if rec_type == 'exon':
                if 'exons' not in db_dict[gene_id][trans_id]:
                    db_dict[gene_id][trans_id]['exons'] = []
                db_dict[gene_id][trans_id]['exons'].append((int(row[3]), int(row[4])))
            elif rec_type == 'start_codon':
                db_dict[gene_id][trans_id]['start_codon'] = ((int(row[3]), int(row[4])))
            else:
                assert False, 'Can\'t get here!, rec_type is: %s' % rec_type
            # print(gene_id)
            # print(trans_id)
            # print(desc)
    print('finished reading gtf')
    print('writing output file')
    with open(output_file, 'w') as out_fd:
        writer = csv.writer(out_fd, delimiter=',')
        writer.writerow(['gene_id', 'trans_id', 'orientation', 'relative_start'])
        for gene_id in db_dict:
            for trans_id in db_dict[gene_id]:
                print('gene: %s, trans: %s' % (gene_id, trans_id))
                orientation = db_dict[gene_id][trans_id]['orientation']
                out_row = [gene_id, trans_id, orientation]
                if 'start_codon' in db_dict[gene_id][trans_id]:
                    if orientation == '+':
                        exons = sorted(db_dict[gene_id][trans_id]['exons'], key=lambda x: x[0])
                        stop_pos = db_dict[gene_id][trans_id]['start_codon'][0]
                        stop_exon_start_idx = 0
                    elif orientation == '-':
                        exons = sorted(db_dict[gene_id][trans_id]['exons'], key=lambda x: x[1], reverse=True)
                        stop_pos = db_dict[gene_id][trans_id]['start_codon'][1]
                        stop_exon_start_idx = 1
                    else:
                        raise Exception('Unknown orientation: %s' % orientation)
                    relative_start = 0
                    for exon in exons:
                        if exon[0] <= stop_pos <= exon[1]:
                            relative_start += abs(stop_pos - exon[stop_exon_start_idx])
                            break
                        else:
                            relative_start += exon[1] - exon[0] + 1
                    out_row.append(relative_start)
                else:
                    out_row.append(0)
                writer.writerow(out_row)
    print('finished writing output file')
            

def main(args):
    gtf_file = args.gtf_file
    output_file = args.output_file
    create_cds_file(gtf_file, output_file)


if __name__ == '__main__':
    args = get_options()
    main(args)